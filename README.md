## Test repo for deploying Flask/WSGI in a Docker container

**Prerequisites:**
- Windows 10/11
- Docker
- WSL2

The code below deploys the Flask app on port 8080 &mdash; change this as desired.

**To run the existing published docker image:**
```bash
docker pull registry.gitlab.developers.cam.ac.uk/ycc39/python-docker
docker run --rm -p 8080:5000 registry.gitlab.developers.cam.ac.uk/ycc39/python-docker

```

**To build and deploy a local container:**
```bash
docker container rm docker-wsgi-server
docker image rm python-docker
docker build -t python-docker .
docker create --name docker-wsgi-server -p 8080:5000 python-docker
docker container start -a docker-wsgi-server

```

**To publish the docker container:**
```bash
docker commit docker-wsgi-server registry.gitlab.developers.cam.ac.uk/ycc39/python-docker
docker push registry.gitlab.developers.cam.ac.uk/ycc39/python-docker

```
