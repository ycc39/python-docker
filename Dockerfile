# syntax=docker/dockerfile:1

FROM python:3.11-slim

WORKDIR /app

COPY . .

RUN python3 -m pip install pip-tools
RUN pip-compile requirements.in
RUN pip-sync
RUN addgroup --system uwsgi
RUN adduser --system uwsgi --ingroup uwsgi

USER uwsgi:uwsgi

CMD ["uwsgi", "--ini", "wsgi.ini"]
